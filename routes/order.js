const express = require("express");
const router = express.Router();
const orderController = require("../controllers/order");
const auth = require("../auth");


// Create order
router.post("/users/checkout", auth.verify, (req, res) => {
	let data = {
		products: req.body.products
	}

    const user = auth.decode(req.headers.authorization)
	orderController.createOrder(user, data).then(resultFromController => res.send(resultFromController));
})

// Retrieve all orders
router.get("/users/orders", auth.verify, (req, res) => {
    const user = auth.decode(req.headers.authorization)
    orderController.getAllOrders(user, req.params).then(resultFromController => res.send(resultFromController))
})

// Retrieve authenticated user order
router.get("/users/myOrders", auth.verify, (req, res) => {
    const user = auth.decode(req.headers.authorization)
    orderController.getOrder(user, req.params).then(resultFromController => res.send(resultFromController))
})

module.exports = router;