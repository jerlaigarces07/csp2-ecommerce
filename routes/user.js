const express = require("express");
const router = express.Router();
const userController = require("../controllers/user");
const auth = require("../auth");

// User registration
router.post("/register", (req, res) => {
    userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
})

// User authentication
router.post("/login", (req, res) => {
    userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})


router.put("/admin/:id", (req, res) => {
	userController.adminAccess(req.params.id)
	.then(resultFromController => res.send(resultFromController));
})

// Set user as admin
router.put("/:userId/setAsAdmin", auth.verify, (req, res) => {

    const user = auth.decode(req.headers.authorization)
    userController.setToAdmin(user, req.params, req.body).then(resultFromController => res.send(resultFromController));
})


module.exports = router;