const express = require("express");
const router = express.Router();
const productController = require("../controllers/product");
const auth = require("../auth");

// Create a product
router.post("/products", auth.verify, (req,res) => {

    const data = auth.decode(req.headers.authorization)

    productController.addProduct(data, req.body).then(resultFromController => res.send(resultFromController));
});

// Retrieve all active products
router.get("/products", (req, res) => {
    productController.getAllActiveProducts().then(resultFromController => res.send(resultFromController))
});

// Retrieve Single Product
router.get("/products/:productId", (req, res) => {
    productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
})

// Update Product Info
router.put("/products/:productId", auth.verify, (req, res) => {

    const user = auth.decode(req.headers.authorization)
    productController.updateProduct(user, req.params, req.body).then(resultFromController => res.send(resultFromController));
})

// Archive Product
router.put("/products/:productId/archive", auth.verify, (req, res) => {

	const user = auth.decode(req.headers.authorization)
	productController.archiveProduct(user, req.params).then(resultFromController => res.send(resultFromController));
	
});

module.exports = router;

