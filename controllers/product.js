const Product = require("../models/Product")
const bcrypt = require("bcrypt");
const auth = require("../auth");

// Create a product
module.exports.addProduct= async (user, reqBody, reqParams, res) => {
    if(user.isAdmin){
        let newProduct = new Product({
            name: reqBody.name,
            description: reqBody.description,
            price: reqBody.price
        })

        return Product.find({name: reqBody.name}).then(result => {
            if(result.length > 0){
                return('Product already exists')
            }
            else{
                return newProduct.save().then((product, error) => {
                    if(error){
                        return false;
                    }
                    else{
                        return ("Successfully added the product");
                    }
                })
            }
        })
    }
    else{
        return ("You do not have access to this feature");
    }

}

// Retrieve all active products
module.exports.getAllActiveProducts = () => {
    return Product.find({isActive: true}).then(result => {
        return result;
    })
}

// Retrieve Single Product
module.exports.getProduct = (reqParams) => {
    return Product.findById(reqParams.productId).then(result => {
        return result;
    })
}

// Update Product Info
module.exports.updateProduct = async (user, reqParams, reqBody) => {
    if(user.isAdmin){
        let updatedProduct = {
            name: reqBody.name,
            description: reqBody.description,
            isActive: reqBody.isActive,
            price: reqBody.price
        }

        return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
            if(error){
                return false;
            }
            else{
                return ("Successfully updated");
            }
        })
        }else{
            return('You do not have access to this feature');
    }
}

// Archive Product
module.exports.archiveProduct = async (user, reqParams) => {

    if(user.isAdmin){
        let updateActiveField = {
            isActive : false
        };

        return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {
            if (error) {
                return false;
            } else {
                return ("Product has been archived");
            }
        });
    }
    else{
        return (`You do not have access to this feature`);
    }
    
};