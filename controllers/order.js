const Order = require("../models/Order")
const bcrypt = require("bcrypt");
const auth = require("../auth");
const Product = require("../models/Product")
const User = require("../models/User")

// Create order
module.exports.createOrder = async (user, data) => {
    let productIds = data.products.map(product => {return {productId: product.productId}})

    if(!user.isAdmin){
        let products = await Product.find({_id: productIds.map((product => product.productId))})
        let activeProducts = products.filter(product => product.isActive)

        if(activeProducts.length == 0){
            return false
        }

        let isUserUpdated = await User.findById(user.id).then(user => {
            const prod = user.products.push(...activeProducts.map(product => {return {productId: product._id.toString()}}));
            return user.save().then((user, error) => {
                if(error){
                    return false;
                }
                else{
                    return true;
                }
            })
        })

        let totalAmount = 0
        activeProducts.forEach(product => {
            totalAmount += product.price
        })

        const newProducts = activeProducts.map(product => {
            return {
                productId: product._id.toString(),
                productName: product.name,
                price: product.price
            }
        })
        console.log("active", newProducts)

        let newOrder = new Order({
            totalAmount: totalAmount,
            purchasedOn: new Date(),
            userId: user.id,
            products: newProducts
        })

        return newOrder.save().then(result => {
            return ('Order Created')
        })

    }
    else{
        return ("You do not have access to this feature");
    }
}

// Retrieve all orders
module.exports.getAllOrders = async (user, req) =>{
    if(user.isAdmin){
        return Order.find({}).then(result => {
            return result;
        })
    }
    else {
        return (`You do not have access to this feature`);
    }
}

// Retrieve authenticated user order
module.exports.getOrder = async (user, req) =>{
    if(!user.isAdmin){
        return Order.find({userId: user.id}).then(result => {
            console.log("RESULT", result)
            if(result.length == 0){
                return ("No order found")
            }
            if(result[0].products.length == 0){
                return ("No order found")
            }
            return result;
        })
    }
    else {
        return (`You do not have access to this feature`);
    }
}