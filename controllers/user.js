const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const userSchema = require('../validation/userSchema');

// User Registration
module.exports.registerUser = async (reqBody) => {
    try {
        const result = await userSchema.validateAsync(reqBody)
    } catch(error) {
        return ({message: "Error: " + error.details[0].message})
    }

    return User.find({email: reqBody.email}).then(result => {
        if(result.length > 0){
            return('User already exists')
        }
        else{
            let newUser = new User({
                firstName: reqBody.firstName,
                lastName: reqBody.lastName,
                email: reqBody.email,
                password: bcrypt.hashSync(reqBody.password, 10)
            })
            return newUser.save().then(result => {
                return [('Registered'), {access: auth.createAccessToken(result)}]
            })
        }
    })
}

// User authentication
module.exports.loginUser = (reqBody) => {
    return User.findOne({email: reqBody.email}).then(result => {
        if(result == null){
            return ("No user found")
        }
        else{
            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
            if(isPasswordCorrect){
                return[("Successfully logged in"), {access: auth.createAccessToken(result)}]
            }
            else{
                return ("Password incorrect");
            }
        }
    })
}

module.exports.adminAccess = (userId) => {
	return User.findById(userId).then((result, error) => {
		if(error){
			return false;
		}
		result.isAdmin = true;

		return result.save().then((updateUser, err) => {
			if(error){
				return false;
			}
			else{
				return (`Granted access as admin`);
			}
		})
	})
}

// Set user as admin
module.exports.setToAdmin = async(user, reqParams) => {
    if(user.isAdmin){
        let updateField = {
            isAdmin : true
        }

        return User.findOne({_id: reqParams.userId}).then(result => {
            if(result.isAdmin){
                return ("User is already an admin")
            }
            else {                
                return User.findByIdAndUpdate(reqParams.userId, updateField).then((user, error) => {
                    if(error){
                        return false;
                    }
                    else{
                        return("Successfully updated user as admin")
                    }
                })
            }
        })
    }
    else{
        return (`You have no access`);
    }
}


