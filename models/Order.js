const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
    totalAmount: {
        type: Number,
        required: [true, "Total Amount is required"]
    },
    purchasedOn: {
        type: Date,
        default: new Date()
    },
    userId: {
        type: String,
        required: [true, "UserID is required"]
    },
    products: [{
        productId: {
            type: String,
            required: [true, "ProductId is required"]
        },
        productName: {
            type: String,
            required: [true, "Product name is required"]
        },
        price: {
            type: Number,
            required: [true, "Price is required"]
        }
    }
    ]
});

module.exports = mongoose.model("Order", orderSchema);