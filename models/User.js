const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
    firstName: {
        type: String
    },
    lastName: {
        type: String
    },
    email: {
        type: String,
        required: [true, "Email is required"]
    },
    password: {
        type: String,
        required: [true, "Password is required"]
    },
    isAdmin: {
        type: Boolean,
        default: false
    },
    products: [{
        productId: {
            type: String,
            required: [true, "Product ID is required"]
        },
        purchasedOn: {
            type: Date,
            default: new Date()
        }
    }
    ]
})

module.exports = mongoose.model("User", userSchema);