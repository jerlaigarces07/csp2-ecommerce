const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoutes = require("./routes/user");
const productRoutes = require("./routes/product")
const orderRoutes = require("./routes/order")

const app = express();

mongoose.connect("mongodb+srv://dbjerliegarces:YMy5dsXKwwDDdWQn@wdc028-course-booking.pqblj.mongodb.net/csp2-ecommerce?retryWrites=true&w=majority",
    {
        useNewUrlParser : true,
        useUnifiedTopology : true
    }
);

mongoose.connection.once('open', () => console.log('Successfully connected to MongoDBAtlas'));

app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.use("/", userRoutes);
app.use("/", productRoutes);
app.use("/", orderRoutes);

app.listen(process.env.PORT || 4004, () => {
    console.log(`Server is up and running on port ${ process.env.PORT || 4004 }`)
});